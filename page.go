package main

type Page struct {
    Title    string
    Template string
    Content  string
    FilePath string
    Uri      string
}
