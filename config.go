package main

import (
    "io/ioutil"
    "log"
    "os"

    "github.com/olebedev/config"
)

const (
    configBlogName          = "blog_name"
    configTheme             = "theme"
    configSrcDirName        = "src_dir_name"
    configOutputDirName     = "output_dir_name"
    configThemeDirName      = "theme_dir_name"

    defaultBlogName          = "avatar-blog"
    defaultTheme             = "default"
    defaultSrcDirName        = "src"
    defaultOutputDirName     = "output"
    defaultThemeDirName      = "theme"

    defaultLayoutTemplateUrl = "https://www.baidu.com" // TODO: Change this shit
)

func CreateConfigFile(filePath string, blogName string) error {
    cfg := map[string]interface{}{
        configBlogName: blogName,
        configTheme: defaultTheme,
        configSrcDirName: defaultSrcDirName,
        configOutputDirName: defaultOutputDirName,
        configThemeDirName: defaultThemeDirName,
    }

    cfgYaml, err := config.RenderYaml(cfg)
    if err != nil {
        return err
    }

    if err := ioutil.WriteFile(filePath + "/avatar_config.yaml", []byte(cfgYaml), 0644); err != nil {
        return err
    }

    return nil
}

func ParseConfigFile(filePath string) (*config.Config, error) {
    var yamlConfig *config.Config

    log.Println("Parsing config file...")
    if _, err := os.Stat("./avatar_config.yaml"); err == nil {
        yamlConfig, err = config.ParseYamlFile("./avatar_config.yaml")
        if err != nil {
            return nil, err
        }
    } else if os.IsNotExist(err) {
        log.Println("avatar_config.yaml not exists, create default config file")
    } else {
        return nil, err
    }

    return yamlConfig, nil
}
