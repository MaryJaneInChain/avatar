package main

import (
    "fmt"
    "io"
    "io/ioutil"
    "os"
    "path"
)

func copyFile(src, dst string) error {
    var err error
    var srcFile *os.File
    var dstFile *os.File
    var srcInfo os.FileInfo

    if srcFile, err = os.Open(src); err != nil {
        return err
    }
    defer srcFile.Close()

    if dstFile, err = os.Create(dst); err != nil {
        return err
    }
    defer dstFile.Close()

    if _, err = io.Copy(dstFile, srcFile); err != nil {
        return err
    }

    if srcInfo, err = os.Stat(src); err != nil {
        return err
    }

    return os.Chmod(dst, srcInfo.Mode())
}

func copyDirectory(src, dst string) error {
    var err error
    var filesInDirectory []os.FileInfo
    var srcinfo os.FileInfo

    if srcinfo, err = os.Stat(src); err != nil {
        return err
    }

    if err = os.MkdirAll(dst, srcinfo.Mode()); err != nil {
        return err
    }

    if filesInDirectory, err = ioutil.ReadDir(src); err != nil {
        return err
    }

    for _, file := range filesInDirectory {
        srcFilePath := path.Join(src, file.Name())
        dstFilePath := path.Join(dst, file.Name())

        if file.IsDir() {
            if err = copyDirectory(srcFilePath, dstFilePath); err != nil {
                fmt.Println(err)
            }
        } else {
            if err = copyFile(srcFilePath, dstFilePath); err != nil {
                fmt.Println(err)
            }
        }
    }

    return nil
}

func makeSlice(args ...interface{}) []interface{} {
    return args
}
