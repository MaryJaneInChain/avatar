package main

import (
    "io/ioutil"
    "time"

    "github.com/olebedev/config"
)

type Article struct {
    Title    string
    Subtitle string
    Template string
    DateTime time.Time
    Content  string
    FilePath string
    Uri      string
    Tags     []string
}

type Articles []Article

func (articles Articles) Len() int {
    return len(articles)
}

func (articles Articles) Less(i, j int) bool {
    return articles[i].DateTime.Before(articles[j].DateTime)
}

func (articles Articles) Swap(i, j int) {
    articles[i], articles[j] = articles[j], articles[i]
}

func newArticle() Article {
    article := Article{
        "Untitled Post",
        "",
        "post",
        time.Now(),
        "",
        "",
        "",
        []string{},
    }

    return article
}

func createBlankArticle(title string, config *config.Config) error {
    dateString := time.Now().Format(dateLayoutISO)
    content := []byte("---\ntitle: " + title + "\nsubtitle: \ntemplate: post\ndate: " + dateString + "\ntags: \n---")
    filePath := "./" + config.UString(configSrcDirName, defaultSrcDirName) + "/" + dateString + "-" + title + ".md"
    err := ioutil.WriteFile(filePath, content, 0644)
    if err != nil {
        return err
    }

    return nil
}
