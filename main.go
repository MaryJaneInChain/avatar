package main

import (
    "os"
)

const (
    dateLayoutISO = "2006-01-02"

    avatarVersion = "0.01"
)

func main() {
    GenerateArgs(os.Args)
}
