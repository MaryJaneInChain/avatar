package main

import (
    "io/ioutil"

    "github.com/olebedev/config"
)

type Webpage struct {
    Title    string
    Template string
    Content  string
    FilePath string
    Uri      string
}

type Webpages []Webpage

func newWebpage() Webpage {
    webpage := Webpage{
        "Untitled Page",
        "page",
        "",
        "",
        "",
    }

    return webpage
}

func createBlankWebpage(title string, config *config.Config) error {
    content := []byte("---\ntitle: " + title + "\nntemplate: page\n---")
    filePath := "./" + config.UString(configSrcDirName, defaultSrcDirName) + "/" + title + ".md"
    err := ioutil.WriteFile(filePath, content, 0644)
    if err != nil {
        return err
    }

    return nil
}
