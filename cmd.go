package main

import (
    "bytes"
    "fmt"
    "html/template"
    "io/ioutil"
	"log"
	"net/http"
    "os"
    "path"
    "path/filepath"
    "sort"
    "strings"
    "time"

    "github.com/gernest/front"
    "github.com/olebedev/config"
    "github.com/shurcooL/github_flavored_markdown"
)

const (
    commandInit     = "init"
    commandRise     = "rise"
    commandVersion  = "version"
    commandHelp     = "help"
    commandNew      = "new"

    subCommandNewArticle = "article"
    subCommandNewWebpage = "page"
)

type Command struct {
    Name            string
    Example         string
    Description     string
}

func GenerateArgs(args []string) error {
    switch {
    case len(args) <= 1:
        ShowVersionInfo()
        ShowHelpInfo()
    case args[1] == commandInit:
        Init(args)
    case args[1] == commandRise:
        Rise(args)
    case args[1] == commandHelp:
        ShowHelpInfo()
    case args[1] == commandVersion:
        ShowVersionInfo()
    case args[1] == commandNew:
        New(args)
    default:
        ShowVersionInfo()
        ShowHelpInfo()
    }

    return nil
}

func Init(args []string) error {
    var blogName, layoutTemplateUrl string

    if len(args) == 2 {
        blogName            = defaultBlogName
        layoutTemplateUrl   = defaultLayoutTemplateUrl
    } else if len(args) == 3 {
        blogName            = args[2]
        layoutTemplateUrl   = defaultLayoutTemplateUrl
    } else {
        blogName            = args[2]
        layoutTemplateUrl   = args[3]
    }

    log.Println("Creating directory " + blogName)
    if _, err := os.Stat("./" + blogName); err == nil {
        log.Println("Directory " + blogName + " already exists, failed")
        return nil
    }
    os.Mkdir("./" + blogName, 0755)

    log.Println("Creating config file: avatar_config.yaml")
    CreateConfigFile("./" + blogName, blogName)

    log.Println("Creating theme directory: " + defaultThemeDirName)
    os.Mkdir("./" + blogName + "/" + defaultThemeDirName, 0755)
    log.Println("Downloading layout template from " + layoutTemplateUrl)
    layout, err := http.Get(layoutTemplateUrl)
    if err != nil {
        return err
    }
    defer layout.Body.Close()

    // TODO：Download and unzip the theme
    log.Println(layout.Body)

    return nil
}

func Rise(args []string) error {
    var yamlConfig *config.Config

    log.Println("Parsing config file...")
    if _, err := os.Stat("./avatar_config.yaml"); err == nil {
        yamlConfig, err = config.ParseYamlFile("./avatar_config.yaml")
        if err != nil {
            return err
        }
    } else if os.IsNotExist(err) {
        log.Println("avatar_config.yaml not exists, create default config file")
    } else {
        return err
    }

    log.Println("Generating articles...")
    articlePaths, err := getOriginalArticlesPath(yamlConfig)
    if err != nil {
        log.Fatal(err)
    }
    articles, err := loadOriginalArticles(articlePaths)
    if err != nil {
        log.Fatal(err)
    }

    log.Println("Generating webpages...")
    webpagePaths, err := getOriginalWebpagesPath(yamlConfig)
    if err != nil {
        log.Fatal(err)
    }
    webpages, err := loadOriginalWebpages(webpagePaths)
    if err != nil {
        log.Fatal(err)
    }

    if err := generateOutput(yamlConfig, articles, webpages); err != nil {
        log.Fatal(err)
    }

    return nil
}

func New(args []string) error {
    config, err := ParseConfigFile("./avatar_config.yaml")
    if err != nil {
        return err
    }

    if len(args) == 3 {
        createBlankArticle(args[2], config)
    } else if len(args) == 4 {
        switch {
        case args[2] == subCommandNewArticle:
            err = createBlankArticle(args[3], config)
        case args[2] == subCommandNewWebpage:
            err = createBlankWebpage(args[3], config)
        default:
            // throw error
        }
    }

    if err != nil {
        return err
    }

    return nil
}

func CheckLayout() bool {
    return true
}

func ShowHelpInfo() error {
    fmt.Println("TODO: Help info")
    return nil
}

func ShowVersionInfo() error {
    fmt.Println("Avatar version " + avatarVersion)
    fmt.Println("In memory of Mark \"The Shark\" Shelton (1957-2018)")

    return nil
}

func getOriginalArticlesPath(config *config.Config) ([]string, error) {
    files, err := filepath.Glob("./" + config.UString(configSrcDirName, defaultSrcDirName) + "/*.md")
    if err != nil {
        return nil, err
    }

    return files, nil
}

func getOriginalWebpagesPath(config *config.Config) ([]string, error) {
    files, err := filepath.Glob("./" + config.UString(configSrcDirName, defaultSrcDirName) + "/*.html")
    if err != nil {
        return nil, err
    }

    return files, nil
}

func loadOriginalArticles(filePaths []string) (Articles, error) {
    articles := Articles{}

    for _, filePath := range filePaths {
        fileName := strings.TrimSuffix(filepath.Base(filePath), filepath.Ext(filePath))

        content, err := ioutil.ReadFile(filePath)
        if err != nil {
            return nil, err
        }

        frontMatter := front.NewMatter()
        frontMatter.Handle("---", front.YAMLHandler)
        front, body, err := frontMatter.Parse(bytes.NewReader(content))
        if err != nil {
            return nil, err
        }

        formatedBody := github_flavored_markdown.Markdown([]byte(body))

        outputUri := fileName + ".html"
        outputFilePath := "output/" + fileName + ".html"

        article := newArticle()
        if title, ok := front["title"]; ok {
            article.Title = title.(string)
        }
        if subtitle, ok := front["subtitle"]; ok {
            if subtitle == nil {
                article.Subtitle = "";
            } else {
                article.Subtitle = subtitle.(string)
            }
        }
        if template, ok := front["template"]; ok {
            article.Template = template.(string)
        }
        if date, ok := front["date"]; ok {
            if datetime, err := time.Parse(dateLayoutISO, date.(string)); err == nil {
                article.DateTime = datetime
            }
        }
        if tags, ok := front["tags"]; ok {
            if tags != nil {
                for _, tag := range tags.([]interface{}) {
                    article.Tags = append(article.Tags, tag.(string))
                }
            }
        }

        article.Content = string(formatedBody)
        article.FilePath = outputFilePath
        article.Uri = outputUri

        articles = append(articles, article)
    }

    sort.Sort(sort.Reverse(articles))

    return articles, nil
}

func loadOriginalWebpages(filePaths []string) ([]Webpage, error) {
    webpages := []Webpage{}

    for _, filePath := range filePaths {
        fileName := strings.TrimSuffix(filepath.Base(filePath), filepath.Ext(filePath))

        content, err := ioutil.ReadFile(filePath)
        if err != nil {
            return nil, err
        }

        frontMatter := front.NewMatter()
        frontMatter.Handle("---", front.YAMLHandler)
        front, body, err := frontMatter.Parse(bytes.NewReader(content))
        if err != nil {
            return nil, err
        }

        outputUri := fileName + ".html"
        outputFilePath := "output/" + fileName + ".html"

        webpage := newWebpage()
        if title, ok := front["title"]; ok {
            webpage.Title = title.(string)
        }
        if template, ok := front["template"]; ok {
            webpage.Template = template.(string)
        }

        webpage.Content = body
        webpage.FilePath = outputFilePath
        webpage.Uri = outputUri

        webpages = append(webpages, webpage)
    }

    return webpages, nil
}

func generateOutput(config *config.Config, articles Articles, webpages []Webpage) error {
    log.Println("Start generating output files...")
    outputDir, err := os.Stat("./" + config.UString(configOutputDirName, defaultOutputDirName))
    if err != nil {
        if os.IsNotExist(err) {
            err = os.Mkdir("./" + config.UString(configOutputDirName, defaultOutputDirName), 0744)
            if err != nil {
                return err
            }
        }
    } else if outputDir.IsDir() {
        cleanOutput()
    } else {
        err := os.RemoveAll("./" + config.UString(configOutputDirName, defaultOutputDirName))
        if err != nil {
            return err
        }
        err = os.Mkdir("./" + config.UString(configOutputDirName, defaultOutputDirName), 0744)
        if err != nil {
            return err
        }
    }

    tpl, err := template.New("").Funcs(template.FuncMap{
        "makeSlice": makeSlice,
    }).ParseGlob("theme/default/*.tmpl")
    if err != nil {
        return err
    }

    err = generateIndexPage(config, tpl, articles)
    if err != nil {
        return err
    }

    err = generateTagIndexPage(config, tpl, articles)
    if err != nil {
        return err
    }

    for _, article := range articles {
        outputFile, err := os.OpenFile(article.FilePath, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
        if err != nil {
            return err
        }
        defer outputFile.Close()

        err = tpl.ExecuteTemplate(
            outputFile,
            article.Template,
            map[string]interface{}{
                "Config":       config,
                "PageTitle":    article.Title,
                "PageSubtitle": article.Subtitle,
                "DateTime":     article.DateTime,
                "Content":      template.HTML(article.Content),
                "Tags":         article.Tags,
            },
        )
        if err != nil {
            return err
        }
    }

    for _, webpage := range webpages {
        outputFile, err := os.OpenFile(webpage.FilePath, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
        if err != nil {
            return err
        }
        defer outputFile.Close()

        err = tpl.ExecuteTemplate(
            outputFile,
            webpage.Template,
            map[string]interface{}{
                "Config":       config,
                "PageTitle":    webpage.Title,
                "Content":      template.HTML(webpage.Content),
            },
        )
        if err != nil {
            return err
        }
    }

    err = generateResources(config)
    if err != nil {
        return err
    }

    /*
    copyDirectory("./theme/default/resource/css", "./output/css")
    copyDirectory("./theme/default/resource/webfonts", "./output/webfonts")
    */

    return nil
}

func generateIndexPage(config *config.Config, tpl *template.Template, articles Articles) error {
    outputFile, err := os.OpenFile("./output/index.html", os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
    if err != nil {
        return err
    }
    defer outputFile.Close()

    err = tpl.ExecuteTemplate(
        outputFile,
        "index",
        map[string]interface{}{
            "Config":   config,
            "Articles": articles,
        },
    )
    if err != nil {
        return err
    }

    return nil
}

func generateTagIndexPage(config *config.Config, tpl *template.Template, articles Articles) error {
    tagArticlesMap := make(map[string][]*Article)

    for k, article := range articles {
        for _, tag := range article.Tags {
            tagArticlesMap[tag] = append(tagArticlesMap[tag], &articles[k])
        }
    }

    err := os.Mkdir("./output/tag", 0744)
    if err != nil {
        return err
    }

    for tag, tagArticles := range tagArticlesMap {
        outputFile, err := os.OpenFile("./output/tag/"+tag+".html", os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
        if err != nil {
            return err
        }
        defer outputFile.Close()

        err = tpl.ExecuteTemplate(
            outputFile,
            "index",
            map[string]interface{}{
                "Config":   config,
                "Articles": tagArticles,
            },
        )
        if err != nil {
            return err
        }
    }

    return nil
}

func generateResources(config *config.Config) error {
    dstResourcePath := config.UString(configOutputDirName, defaultOutputDirName)
    themeResourcePath := path.Join(
        config.UString(configThemeDirName, defaultThemeDirName),
        config.UString(configTheme, defaultTheme),
        "resource",
    )

    if resourceFile, err := os.Stat(themeResourcePath); os.IsNotExist(err) {
        return nil
    } else if !resourceFile.IsDir() {
        return nil
    } else if err != nil {
        return err
    }

    if filesInResourceDirectory, err := ioutil.ReadDir(themeResourcePath); err != nil {
        return nil
    } else {
        for _, file := range filesInResourceDirectory {
            srcFilePath := path.Join(themeResourcePath, file.Name())
            dstFilePath := path.Join(dstResourcePath, file.Name())

            if file.IsDir() {
                if err = copyDirectory(srcFilePath, dstFilePath); err != nil {
                    return err
                }
            } else {
                if err = copyFile(srcFilePath, dstFilePath); err != nil {
                    return err
                }
            }
        }
    }

    srcResourcePath := path.Join(
        config.UString(configSrcDirName, defaultSrcDirName),
        "resource",
    )

    if resourceFile, err := os.Stat(srcResourcePath); os.IsNotExist(err) {
        return nil
    } else if !resourceFile.IsDir() {
        return nil
    } else if err != nil {
        return err
    }

    if filesInResourceDirectory, err := ioutil.ReadDir(srcResourcePath); err != nil {
        return nil
    } else {
        for _, file := range filesInResourceDirectory {
            srcFilePath := path.Join(srcResourcePath, file.Name())
            dstFilePath := path.Join(dstResourcePath, file.Name())

            if file.IsDir() {
                if err = copyDirectory(srcFilePath, dstFilePath); err != nil {
                    return err
                }
            } else {
                if err = copyFile(srcFilePath, dstFilePath); err != nil {
                    return err
                }
            }
        }
    }

    return nil
}

func cleanOutput() error {
    files, err := filepath.Glob("./output/*")
    if err != nil {
        return err
    }
    for _, f := range files {
        if err := os.RemoveAll(f); err != nil {
            return err
        }
    }

    return nil
}
